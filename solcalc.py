from fipy import *
from time import sleep
import numpy as np
from numpy import *
from fipy.tools.numerix import sin, cos, dot
import copy
from fipy.tools.dump import read, write
import datetime
from time import gmtime, strftime, localtime, time, sleep
import json
import io
import sys
import os
import fcntl



def printlog(text):
    print('%s %s'%(strftime("[%H:%M:%S]", localtime()) ,text))


class calcParam:
    def __init__(self,Reps,Rm,zeps,zm,meshdelta,sweepres):
        self.Reps = Reps
        self.Rm = Rm
        self.zeps = zeps
        self.zm = zm
        self.meshdelta = meshdelta
        self.sweepres = sweepres
    def pr(self):
        printlog('Calculation parameters: zeps=%.3f, Rm=%.3f, meshdelta=%.4f'%(self.zeps,self.Rm,self.meshdelta))



class modelParam:
    def __init__(self,L,M5tL,mq,xitL,Mbulk2tL2,chicoeff,gamma=None):
        self.L = L
        self.M5tL = M5tL
        self.mqtL = mq * L
        self.xitL = xitL
        self.Mbulk2tL2 = Mbulk2tL2
        self.chicoeff = chicoeff
        self.Nc = 3.
        if gamma is None:
          self.gamma=self.Nc/(16.*pi**2*M5tL)
        else:
          self.gamma=gamma
        self.chicoeff = chicoeff
    def pr(self):
        printlog('Model parameters: L^-1=%.3f MeV, mq=%.3f MeV, xitL=%.3f, MbL2=%.3f, gamma=%.3f'%(1./self.L,
                        self.mqtL/self.L,self.xitL,self.Mbulk2tL2,self.gamma))


class profileGen:
    def __init__(self,cp,mp):
        self.alpha = sqrt(4.+mp.Mbulk2tL2)
        self.cp = 1./(1-cp.zeps**(2.*self.alpha)) * (mp.xitL - mp.mqtL)
        self.cm = 1./(1-cp.zeps**(2.*self.alpha)) * (mp.mqtL - mp.xitL * cp.zeps**(2.*self.alpha))
    def chi0(self,z):
        return 2.*(self.cp*z**(2.+self.alpha) + self.cm*z**(2.-self.alpha))
    def dzchi0(self,z):
        return 2.*(self.cp*(2.+self.alpha)*z**(1.+self.alpha) + self.cm*(2.-self.alpha)*z**(1.-self.alpha))

class coordConv:
    def __init__(self,cp,mesh):
        self.zeps = cp.zeps
        self.Reps = cp.Reps
        self.dz = mesh.dy
        self.dr = mesh.dx
        self.nz = mesh.ny
        self.nr = mesh.nx
    def itor(self,i):
        if i >= self.nr:
           print 'itor error: i=%i exceeds nr=%i'%(i,self.nr)
        return self.Reps + (i+0.5)*self.dr
    def jtoz(self,j):
        if j >= self.nz:
           print 'jtoz error: j=%i exceeds nz=%i'%(j,self.nz)
        return self.zeps + (j+0.5)*self.dz
    def rtoi(self,r):
        if r > self.Reps + self.dr * self.nr:
           print 'rtoi error: r=%g exceeds the boundaries, with Reps=%g, dr = %g and nr = %i'%(r,self.Reps,self.dr,self.nr)
        i = int((r-self.Reps)/self.dr)
        if i == self.nr: i -= 1
        return i
    def ztoj(self,z):
        if z > self.zeps + self.dz * self.nz:
           print 'ztoj error: z=%g exceeds the boundaries, with zeps=%g, dz = %g and nz = %i'%(z,self.zeps,self.zr,self.nz)
        j = int((z-self.zeps)/self.dz)
        if j == self.nz: j -= 1
        return j
    def ijton(self,i,j):
        return j*self.nr+i


class vis1d:
    def __init__(self,cp,axis,var,coord,plot=True):
        if not (axis=='r' or axis=='z'):
           print 'unknown axis name %s'%axis
           return
        self.meshdelta = cp.meshdelta
        if (axis=='r'):
           self.mesh1d = Grid1D(dx=cp.meshdelta,nx=int((cp.Rm-cp.Reps)/cp.meshdelta))
           self.r1dv = self.mesh1d.cellCenters
           self.z1dv = coord*ones(self.mesh1d.numberOfCells)
        else:
           self.mesh1d = Grid1D(dx=cp.meshdelta,nx=int((cp.zm-cp.zeps)/cp.meshdelta))
           self.r1dv = coord*ones(self.mesh1d.numberOfCells)
           self.z1dv = self.mesh1d.cellCenters
        self.initvar = var
        self.var1d = CellVariable(mesh=self.mesh1d)
        self.var1d.name = var.name
        self.var1d.setValue(self.initvar((self.r1dv,self.z1dv),order=0))
        self.plot = plot
        if self.plot: self.viewer1d = Viewer(vars=self.var1d)

    def plot(self):
        if not self.plot: return
        self.var1d.setValue(self.initvar((self.r1dv,self.z1dv),order=0))
        self.viewer1d.plot()

    def integrate(self):
        self.var1d.setValue(self.initvar((self.r1dv,self.z1dv),order=0))
        return sum(self.var1d.numericValue)*self.meshdelta

class calcWrapper:

    def save(self,filename):
	saveinfo=(self.cp,self.mp,self.psi1,self.psi2,self.Ar,self.Az,self.s,self.chi1,self.chi2)
        write(saveinfo,filename)
        printlog('Saved as %s'%filename)

    def __init__(self,cp=None,mp=None,filename=None,cwold=None):



        self.plots=[]

        if filename is not None:
            saveinfo=read(filename)
            self.cp = copy.deepcopy(saveinfo[0])
            self.mp = copy.deepcopy(saveinfo[1])
            self.mesh = saveinfo[2].mesh
        else:
            self.cp = copy.deepcopy(cp)
            self.mp = copy.deepcopy(mp)
            nx = int((self.cp.Rm-self.cp.Reps)/self.cp.meshdelta)
            ny = int((self.cp.zm-self.cp.zeps)/self.cp.meshdelta)
            self.mesh = Grid2D(dx=(self.cp.Rm-self.cp.Reps)/nx, dy=(self.cp.zm-self.cp.zeps)/ny,nx=nx,ny=ny)

        self.pg = profileGen(self.cp,self.mp)

        self.Lfem = self.mp.L * 197.3

        self.r = CellVariable(name = "r coord", mesh=self.mesh,
               value=self.mesh.cellCenters[0] + self.cp.Reps)
        self.z = CellVariable(name = "z coord", mesh=self.mesh,
               value=self.mesh.cellCenters[1] + self.cp.zeps)

        self.rgradfix = CellVariable(mesh=self.mesh,value=0.,rank=1)
        #rgradfix[0] = 1./r.grad.numericValue[0]
        self.rgradfix[0] = 1.


        self.rgradcalc = CellVariable(mesh=self.mesh,value=0.,rank=1)
        self.rgradcalc[0] = 1./self.r.grad.numericValue[0]

        self.zgradfix = CellVariable(mesh=self.mesh,value=0.,rank=1)
        #zgradfix[1] = 1./z.grad.numericValue[1]
        self.zgradfix[1] = 1.


        self.zgradcalc = CellVariable(mesh=self.mesh,value=0.,rank=1)
        self.zgradcalc[1] = 1./self.z.grad.numericValue[1]

        #self.rgradfix = self.rgradcalc
        #self.zgradfix = self.zgradcalc
        #self.rgradfix = self.r.grad
        #self.zgradfix = self.z.grad
        self.rgradcalc = [[1],[0]]
        self.zgradcalc = [[0],[1]]

        self.rgradfix = [[1],[0]]
        self.zgradfix = [[0],[1]]

        solitonrad = 0.5

        def infphase(z):
            return pi * (z - self.cp.zeps)/(self.cp.zm-self.cp.zeps)

        from scipy.special import erf

        #def rprof1byr(r):
        #    return erf(solitonrad*r)/r
            #return 2./self.cp.Rm - r/(self.cp.Rm)**2
        def rprof1(r):
            return erf(solitonrad*r) + r / self.cp.Rm * (1.-erf(solitonrad*self.cp.Rm))
            #return 1.-(1.-r/self.cp.Rm)**2
        def rprof1byr(r): return rprof1(r) / r

        #def rprof2byr(r):
        #    return erf(solitonrad*r)**2/r
            #return 6./self.cp.Rm**3 * (r / 2. * self.cp.Rm - r**2 / 3.)
        def rprof2(r):
            return erf(solitonrad*r)**2 + r / self.cp.Rm * (1.-erf(solitonrad*self.cp.Rm)**2)
            #return 6./self.cp.Rm**3 * (r**2 / 2. * self.cp.Rm - r**3 / 3.)
        def rprof2byr(r): return rprof2(r)/r


        if filename is None:
            # variables
            self.psi1 = CellVariable(name = "psi 1",mesh = self.mesh, hasOld=1)
            self.psi2 = CellVariable(name = "psi 2",mesh = self.mesh, hasOld=1)

            self.Ar = CellVariable(name = "A r",mesh = self.mesh, hasOld=1)
            self.Az = CellVariable(name = "A z",mesh = self.mesh, hasOld=1)
            self.s = CellVariable(name = "s",mesh = self.mesh, hasOld=1)
            self.chi1 = CellVariable(name = "chi 1",mesh = self.mesh, hasOld=1)
            self.chi2 = CellVariable(name = "chi 2",mesh = self.mesh, hasOld=1)

            # Setting initial values

            if cwold is None:


                self.psi1.setValue(sin(infphase(self.z)) * rprof1byr(self.r))
                self.psi2.setValue((1.-cos(infphase(self.z)))*rprof2byr(self.r))
                #self.Ar.setValue(sin(infphase(self.z))*cos(pi*self.r/self.cp.Rm) / self.cp.Rm)
                #self.Ar.setValue(sin(infphase(self.z)) * solitonrad * (1-erf(0.5*self.r)**2) )
                self.Ar.setValue(sin(infphase(self.z)) * solitonrad * (1-rprof2(self.r)) )
                #self.Az.setValue(pi/(self.cp.zm-self.cp.zeps) * erf(solitonrad*self.r) )
                self.Az.setValue(pi/(self.cp.zm-self.cp.zeps) * rprof1(self.r) )
                #self.s.setValue(0.)
                self.s.setValue(-sin(infphase(self.z)/2.) * 4. * erf(solitonrad*self.r)*(1-erf(solitonrad*self.r)))
                self.chi1.setValue(-sin(infphase(self.z))*self.pg.chi0(self.z)*rprof1(self.r))
                self.chi2.setValue( cos(infphase(self.z))*self.pg.chi0(self.z))
            else:
                self.interpolatedata(cwold)

        else:
            self.psi1 = saveinfo[2]
            self.psi2 = saveinfo[3]
            self.Ar = saveinfo[4]
            self.Az = saveinfo[5]
            self.s = saveinfo[6]
            self.chi1 = saveinfo[7]
            self.chi2 = saveinfo[8]

        # Setting constraints

        self.rf, self.zf = self.mesh.faceCenters
        self.rf = self.rf + self.cp.Reps
        self.zf = self.zf + self.cp.zeps

        # z = 0
        self.psi1.constrain(0.,self.mesh.facesBottom)
        self.psi2.constrain(0.,self.mesh.facesBottom)
        self.Ar.constrain(0.,self.mesh.facesBottom)
        self.s.constrain(0.,self.mesh.facesBottom)
        self.chi1.constrain(0.,self.mesh.facesBottom)
        self.chi2.constrain(self.pg.chi0(self.cp.zeps),self.mesh.facesBottom)

        # r = R

        self.psi1.constrain(sin(infphase(self.zf))/self.cp.Rm,self.mesh.facesRight)
        self.psi2.constrain((1.-cos(infphase(self.zf)))/self.cp.Rm,self.mesh.facesRight)
        self.Az.constrain(pi/(self.cp.zm-self.cp.zeps),self.mesh.facesRight)
        self.s.constrain(0.,self.mesh.facesRight)
        self.chi1.constrain(-sin(infphase(self.zf))*self.pg.chi0(self.zf),self.mesh.facesRight)
        self.chi2.constrain( cos(infphase(self.zf))*self.pg.chi0(self.zf),self.mesh.facesRight)

        # z = zm

        self.psi1.constrain(0.,self.mesh.facesTop)
        self.Ar.constrain(0.,self.mesh.facesTop)
        self.chi1.constrain(0.,self.mesh.facesTop)
        self.chi2.constrain(-self.pg.chi0(self.cp.zm),self.mesh.facesTop)

        # r = 0

        self.psi1.constrain(self.Ar.arithmeticFaceValue,self.mesh.facesLeft)
        self.psi2.constrain(0., self.mesh.facesLeft)
        #Ar.faceGrad.constrain(((0,),(0,)),mesh.facesLeft)

        self.Az.constrain(0.,self.mesh.facesLeft)
        self.s.constrain(0.,self.mesh.facesLeft)
        self.chi1.constrain(0.,self.mesh.facesLeft)




        #setting equations

    # obj.f_psi1 = @(r,z,u,ur,uz)...
    #            ... z/zm D_mu zm/z D_mu phi
    #            0-1./z .* (uz(1,:)+u(4,:).*u(2,:)) + (ur(3,:)+uz(4,:)).*u(2,:) + ...
    #            2*(u(3,:).*ur(2,:)+u(4,:).*uz(2,:)) - (u(3,:).^2+u(4,:).^2).*u(1,:)+...
    #            ... 1/r.^2 phi (1-|phi|^2)
    #            1./r.^2.*(1-u(1,:).^2-u(2,:).^2).*u(1,:)+...
    #            ... s-part
    #            mp.gamma*z.*((ur(5,:)./r-u(5,:)./r.^2).*(-uz(2,:)+u(4,:).*u(1,:)) - (uz(5,:)./r).*(-ur(2,:)+u(3,:).*u(1,:)))+...
    #            ...4g_5^2/z^2 chi_2 (phi_2 chi_1 - phi_1 chi_2)
    #            0 - mp.chicoeff*1/2./z.^2.*u(7,:).*(u(1,:).*u(7,:) - u(2,:).*u(6,:));

    # -iA_mu D_mu phi1 = - A^2 phi_1 + A_mu d_mu phi_2
    # d_mu D_mu phi1 = r d_mu (A_mu psi2) + Ar/r psi2 - dm Am / r

        self.eqphi1n = TransientTerm(var=self.psi1) == \
             DiffusionTerm(coeff=1.,var=self.psi1) + 2.*dot(self.psi1.grad,self.rgradfix)/self.r \
           - dot(self.psi1.grad,self.zgradfix)/self.z - (self.r*self.psi2-1.) * self.Az / self.z / self.r \
           + (dot(self.Ar.grad,self.rgradfix) + dot(self.Az.grad,self.zgradfix))*(self.r*self.psi2-1.)/self.r \
           + 2.*dot(self.psi2.grad,self.rgradfix)*self.Ar + 2.*self.Ar*self.psi2/self.r \
           + 2.*dot(self.psi2.grad,self.zgradfix)*self.Az \
           + ImplicitSourceTerm(coeff=-(self.Ar**2+self.Az**2),var=self.psi1) \
           + ImplicitSourceTerm(coeff=-(self.r**2*self.psi1**2+(self.r*self.psi2-1.)**2)/self.r**2,var=self.psi1) + self.psi1/self.r**2 \
           + self.mp.gamma*self.z*((dot(self.s.grad,self.rgradfix)/self.r-self.s/self.r**2)*(-dot(self.psi2.grad,self.zgradfix)+self.Az*self.psi1) \
             - dot(self.s.grad,self.zgradfix)/self.r*(-dot(self.psi2.grad,self.rgradfix)-self.psi2/self.r+self.Ar*self.psi1)) \
           + self.mp.chicoeff * ImplicitSourceTerm(-1./(2.*self.z**2)*self.chi2**2,var=self.psi1) \
           - self.mp.chicoeff * 1./(2.*self.z**2)*self.chi2*(- self.psi2*self.chi1 + self.chi1/self.r)




    # obj.f_psi2 = @(r,z,u,ur,uz)...
    #            ... z/zm D zm/z D phi
    #            0-1./z .* (uz(2,:)-u(4,:).*u(1,:)) - (ur(3,:)+uz(4,:)).*u(1,:) + ...
    #            0-2*(u(3,:).*ur(1,:)+u(4,:).*uz(1,:)) - (u(3,:).^2+u(4,:).^2).*u(2,:)+...
    #            ... 1/r.^2 phi (1-|phi|^2)
    #            1./r.^2.*(1-u(1,:).^2-u(2,:).^2).*u(2,:)+...
    #            ... s-part
    #            mp.gamma*z.*((ur(5,:)./r-u(5,:)./r.^2).*(uz(1,:)+u(4,:).*u(2,:)) - (uz(5,:)./r).*(ur(1,:)+u(3,:).*u(2,:)))+...
    #            ... - 4g_5^2/z^2 chi_1 (phi_2 chi_1 - phi_1 chi_2)
    #            mp.chicoeff*1/2./z.^2.*u(6,:).*(u(1,:).*u(7,:) - u(2,:).*u(6,:));


        self.eqphi2n = TransientTerm(var=self.psi2) == \
             DiffusionTerm(coeff=1.,var=self.psi2) + 2.*dot(self.psi2.grad,self.rgradfix)/self.r \
           - dot(self.psi2.grad,self.zgradfix)/self.z + self.psi1 * self.Az / self.z \
           - (dot(self.rgradfix,self.Ar.grad)+dot(self.zgradfix,self.Az.grad))*self.psi1 \
           - 2.*dot(self.psi1.grad,self.rgradfix)*self.Ar - 2.*self.Ar*self.psi1/self.r \
           - 2.*dot(self.psi1.grad,self.zgradfix)*self.Az \
           + ImplicitSourceTerm(coeff=-(self.Ar**2+self.Az**2),var=self.psi2) + (self.Ar**2+self.Az**2)/self.r \
           + ImplicitSourceTerm(coeff=-(self.r**2*self.psi1**2+(self.r*self.psi2-1.)**2)/self.r**2,var=self.psi2) \
             + (self.r**2*self.psi1**2+(self.r*self.psi2-1.)**2)/self.r**2 / self.r + (self.psi2*self.r-1.)/self.r**2/self.r \
           + self.mp.gamma*self.z*((dot(self.s.grad,self.rgradfix)/self.r-self.s/self.r**2)*(dot(self.psi1.grad,self.zgradfix)+self.Az*(self.r*self.psi2-1)/self.r) \
             - dot(self.s.grad,self.zgradfix)/self.r*(dot(self.psi1.grad,self.rgradfix) + self.psi1/self.r +self.Ar*(self.r*self.psi2-1)/self.r)) \
           + self.mp.chicoeff * 1./(2.*self.z**2)*self.chi1*(self.psi1*self.chi2 + self.chi1/self.r) \
           + self.mp.chicoeff * ImplicitSourceTerm(coeff=-1./(2.*self.z**2)*self.chi1**2,var=self.psi2)



    # obj.f_Ar = @(r,z,u,ur,uz)...
    #            ... z/r^2zm d r^2 zm/z (F)
    #            0 - 1./z .* (uz(3,:) - ur(4,:)) + ...
    #            ... -1/r^2 (i phi* D phi + hc)
    #            0 - 2./r.^2 .* (u(3,:).*(u(1,:).^2+u(2,:).^2)-u(1,:).*ur(2,:)+u(2,:).*ur(1,:))+...
    #            ... s-part
    #            mp.gamma*z./r.^2.*(uz(5,:)./r).*(1-u(1,:).^2-u(2,:).^2)+...
    #            ... chi-part
    #            mp.chicoeff*1/2./(z.^2).*(u(6,:).*ur(7,:)-u(7,:).*ur(6,:)-u(3,:).*(u(6,:).^2+u(7,:).^2));

        self.eqArn = TransientTerm(var=self.Ar) == DiffusionTerm(coeff=1.,var=self.Ar) \
         - 1./self.z*(dot(self.zgradfix,self.Ar.grad)-dot(self.rgradfix,self.Az.grad)) \
         + ImplicitSourceTerm(coeff=-2.*(self.r**2*self.psi1**2+(self.r*self.psi2-1.)**2)/self.r**2,var=self.Ar) \
         - 2./self.r**2*(-self.r*self.psi1*(self.r*dot(self.rgradfix,self.psi2.grad) + self.psi2)+(self.r*self.psi2-1.)*(self.r*dot(self.rgradfix,self.psi1.grad)+self.psi1)) \
         + self.mp.gamma*self.z/self.r**2*(dot(self.zgradfix,self.s.grad)/self.r)*(1.-(self.r**2*self.psi1**2+(self.r*self.psi2-1.)**2)) \
         + self.mp.chicoeff * (ImplicitSourceTerm(coeff=-(self.chi1**2+self.chi2**2)/(2.*self.z**2),var=self.Ar) \
         - self.mp.chicoeff * 1./(2.*self.z**2)*(-self.chi1*dot(self.rgradfix,self.chi2.grad)+self.chi2*(dot(self.rgradfix,self.chi1.grad))))



    # obj.f_Az = @(r,z,u,ur,uz)...
    #            ... z/r^2zm d r^2 zm/z (F)
    #            2./r.*(ur(4,:)-uz(3,:)) + ...
    #            ... -1/r^2 (i phi* D phi + hc)
    #            0-2./r.^2 .* (u(4,:).*(u(1,:).^2+u(2,:).^2)-u(1,:).*uz(2,:)+u(2,:).*uz(1,:))+...
    #            ... s-part
    #            mp.gamma*z./r.^2.*(ur(5,:)./r-u(5,:)./r.^2).*(u(1,:).^2+u(2,:).^2-1)+...
    #            ... chi-part
    #            mp.chicoeff*1/2./(z.^2).*(u(6,:).*uz(7,:)-u(7,:).*uz(6,:)-u(4,:).*(u(6,:).^2+u(7,:).^2));

        self.eqAzn = TransientTerm(var=self.Az) ==  DiffusionTerm(coeff=1.,var=self.Az) \
         + 2./self.r*(dot(self.rgradfix,self.Az.grad)-dot(self.zgradfix,self.Ar.grad)) \
         + ImplicitSourceTerm(coeff=-2.*(self.r**2*self.psi1**2+(self.r*self.psi2-1.)**2)/self.r**2,var=self.Az) \
         - 2./self.r**2*(-self.r*self.psi1*self.r*dot(self.zgradfix,self.psi2.grad)+(self.r*self.psi2-1.)*self.r*(dot(self.zgradfix,self.psi1.grad))) \
         + self.mp.gamma*self.z/self.r**2*(dot(self.rgradfix,self.s.grad)/self.r - self.s/self.r**2)*((self.r**2*self.psi1**2+(self.r*self.psi2-1.)**2)-1.) \
         + self.mp.chicoeff * (ImplicitSourceTerm(coeff=-(self.chi1**2+self.chi2**2)/(2.*self.z**2),var=self.Az) \
         - self.mp.chicoeff * 1./(2.*self.z**2)*(-self.chi1*dot(self.zgradfix,self.chi2.grad)+self.chi2*(dot(self.zgradfix,self.chi1.grad))))


    # obj.f_s = @(r,z,u,ur,uz)...
    #            ... -1/z d_z s
    #            0-1./z.*uz(5,:) + ...
    #            ... (1-phi^2)e_mn d_m A_n
    #            0-mp.gamma*z./r.*((1-u(1,:).^2-u(2,:).^2).*(ur(4,:)-uz(3,:))) + ...
    #            ... -2 d_m |phi|^2 A_n e_mn
    #            2*mp.gamma*z./r.*((u(1,:).*ur(1,:)+u(2,:).*ur(2,:)).*u(4,:) - (u(1,:).*uz(1,:)+u(2,:).*uz(2,:)).*u(3,:)) + ...
    #            ... +2 e^mn d_m phi1 d_n phi2
    #            0-2*mp.gamma*z./r.*(ur(1,:).*uz(2,:)-uz(1,:).*ur(2,:));

        self.eqsn = TransientTerm(var=self.s) == DiffusionTerm(coeff=1.,var=self.s) \
        - dot(self.zgradfix,self.s.grad)/self.z \
        - self.mp.gamma*self.z/self.r*(1.-(self.r**2*self.psi1**2+(self.r*self.psi2-1)**2))*(dot(self.rgradfix,self.Az.grad) - dot(self.zgradfix,self.Ar.grad)) \
        + 2.*self.mp.gamma*self.z/self.r*((self.r*self.psi1*(self.r*dot(self.rgradfix,self.psi1.grad)+self.psi1) + (self.r*self.psi2-1.)*(self.r*dot(self.rgradfix,self.psi2.grad)+self.psi2))*self.Az - (self.r*self.psi1*self.r*dot(self.zgradfix,self.psi1.grad) + (self.r*self.psi2-1.)*self.r*dot(self.zgradfix,self.psi2.grad))*self.Ar) \
        - 2.*self.mp.gamma*self.z/self.r*((self.r*dot(self.rgradfix,self.psi1.grad) + self.psi1)*(self.r*dot(self.zgradfix,self.psi2.grad)) - self.r*dot(self.zgradfix,self.psi1.grad)*(self.r*dot(self.rgradfix,self.psi2.grad)+self.psi2))


    # obj.f_chi1 = @(r,z,u,ur,uz)...
    #            ... 2/r D_r chi_1 - 3_z D_z chi_1
    #             2./r.*(ur(6,:)+u(3,:).*u(7,:)) - 3./z.*(uz(6,:)+u(4,:).*u(7,:))+ ...
    #            ... d_mu A_mu chi_2 + 2 A_mu d_mu chi_2 - A_mu^2 chi_1
    #            ((ur(3,:)+uz(4,:)).*u(7,:) + 2*(u(3,:).*ur(7,:)+u(4,:).*uz(7,:)) - (u(3,:).^2 + u(4,:).^2).*u(6,:)) + ...
    #            ... 2/r^2 phi_2 (phi_1 chi_2 - chi_1 phi_2) + 3/z^2 chi_1
    #            mp.debugcoeff1 * 2./r.^2 .* u(2,:).*(u(1,:).*u(7,:)-u(2,:).*u(6,:)) + ...
    #            0 - mp.Mbulk2tL2./z.^2 .* u(6,:);

        self.eqchi1n = TransientTerm(var=self.chi1) == DiffusionTerm(coeff=1.,var=self.chi1) \
           + 2./self.r*(dot(self.rgradfix,self.chi1.grad)+self.Ar*self.chi2) - 3./self.z*(dot(self.zgradfix,self.chi1.grad)+self.Az*self.chi2) \
           + (dot(self.rgradfix,self.Ar.grad)+dot(self.zgradfix,self.Az.grad))*self.chi2 \
           + 2.*(dot(self.rgradfix,self.chi2.grad)*self.Ar + dot(self.zgradfix,self.chi2.grad)*self.Az) \
           + ImplicitSourceTerm(coeff=-(self.Ar**2+self.Az**2),var=self.chi1) \
           + 2./self.r**2 * (self.r*self.psi2-1.)*(self.r*self.psi1*self.chi2) \
           + ImplicitSourceTerm(coeff=-2./self.r**2 * (self.r*self.psi2-1.)**2,var=self.chi1) \
           - self.mp.Mbulk2tL2/self.z**2 * self.chi1


    # obj.f_chi2 = @(r,z,u,ur,uz)...
    #            ... 2/r D_r chi_2 - 3_z D_z chi_2
    #            2./r.*(ur(7,:)-u(3,:).*u(6,:)) - 3./z.*(uz(7,:)-u(4,:).*u(6,:))+ ...
    #            ... - d_mu A_mu chi_1 - 2 A_mu d_mu chi_1 - A_mu^2 chi_2
    #            (0 - (ur(3,:)+uz(4,:)).*u(6,:) - 2*(u(3,:).*ur(6,:)+u(4,:).*uz(6,:)) - (u(3,:).^2 + u(4,:).^2).*u(7,:)) + ...
    #            ... - 2/r^2 phi_1 (phi_1 chi_2 - chi_1 phi_2) + 3/z^2 chi_2
    #            mp.debugcoeff1 *(0 - 2./r.^2 .* u(1,:).*(u(1,:).*u(7,:)-u(2,:).*u(6,:))) - mp.Mbulk2tL2./z.^2 .* u(7,:);

        self.eqchi2n = TransientTerm(var=self.chi2) == DiffusionTerm(coeff=1.,var=self.chi2) \
           + 2./self.r*(dot(self.rgradfix,self.chi2.grad)-self.Ar*self.chi1) - 3./self.z*(dot(self.zgradfix,self.chi2.grad)-self.Az*self.chi1) \
           - (dot(self.rgradfix,self.Ar.grad)+dot(self.zgradfix,self.Az.grad))*self.chi1 \
           - 2.*(dot(self.rgradfix,self.chi1.grad)*self.Ar + dot(self.zgradfix,self.chi1.grad)*self.Az) \
           + ImplicitSourceTerm(coeff=-(self.Ar**2+self.Az**2),var=self.chi2) \
           + ImplicitSourceTerm(coeff=-2./self.r**2 * (self.r*self.psi1)**2,var=self.chi2) \
           + 2./self.r**2 * (self.r*self.psi1)*( (self.r*self.psi2-1.)*self.chi1) \
           - self.mp.Mbulk2tL2/self.z**2 * self.chi2


        if self.mp.chicoeff == 0:
           self.eqsystemn = self.eqphi1n & self.eqphi2n & self.eqArn & self.eqAzn & self.eqsn
        else:
           self.eqsystemn = self.eqphi1n & self.eqphi2n & self.eqArn & self.eqAzn & self.eqsn & self.eqchi1n & self.eqchi2n

        self.phi1 = self.psi1 * self.r
        self.phi2 = self.psi2 * self.r - 1.
        self.phi1.name = 'phi 1 (secondary variable)'
        self.phi2.name = 'phi 2 (secondary variable)'

        # d_r A_r + d_z A_z
        self.gauge = dot(self.rgradfix,self.Ar.grad) + dot(self.zgradfix,self.Az.grad)
        self.gauge.name = 'gauge (drAr+dzAz)'

        # 4(dr phi1 dz phi2 - dr phi2 dz phi1) - 2e_mn dm An (phi1^2 + phi2^2 - 1)
        # - 4e_mn A_n (phi1 dm phi1 + phi2 dm phi2)
        self.topcharge = 4.*(dot(self.rgradcalc,self.phi1.grad)*dot(self.zgradcalc,self.phi2.grad) - dot(self.rgradcalc,self.phi2.grad)*dot(self.zgradcalc,self.phi1.grad)) \
              - 2.*(dot(self.rgradcalc,self.Az.grad)-dot(self.zgradcalc,self.Ar.grad))*(self.phi1**2+self.phi2**2-1.) \
              - 4.*(self.Az*(self.phi1*dot(self.rgradcalc,self.phi1.grad) + self.phi2*dot(self.rgradcalc,self.phi2.grad))-self.Ar*(self.phi1*dot(self.zgradcalc,self.phi1.grad) + self.phi2*dot(self.zgradcalc,self.phi2.grad)))
        self.topcharge.name = 'topological charge density'


        self.energyphi = 8.*pi*self.mp.M5tL/self.z*((dot(self.rgradcalc,self.phi1.grad)+self.Ar*self.phi2)**2 + (dot(self.zgradcalc,self.phi1.grad)+self.Az*self.phi2)**2 \
              + (dot(self.rgradcalc,self.phi2.grad)-self.Ar*self.phi1)**2 + (dot(self.zgradcalc,self.phi2.grad)-self.Az*self.phi1)**2 \
              + self.r**2/2.*(dot(self.rgradcalc,self.Az.grad) - dot(self.zgradcalc,self.Ar.grad))**2 \
              + 1./(2.*self.r**2) *(1-self.phi1**2-self.phi2**2)**2 -1./2.*(dot(self.rgradcalc,self.s.grad)**2 + dot(self.zgradcalc,self.s.grad)**2))

        self.energyCS = - 8.*pi*self.mp.M5tL * self.mp.gamma/2. * self.s/self.r * ( \
                4.*(dot(self.rgradcalc,self.phi1.grad)*dot(self.zgradcalc,self.phi2.grad) - dot(self.zgradcalc,self.phi1.grad)*dot(self.rgradcalc,self.phi2.grad)) \
              + 2.*(dot(self.rgradcalc,self.Az.grad)-dot(self.zgradcalc,self.Ar.grad))*(1-self.phi1**2-self.phi2**2) \
              - 4.*(self.Az*(self.phi1*dot(self.rgradcalc,self.phi1.grad) + self.phi2*dot(self.rgradcalc,self.phi2.grad))-self.Ar*(self.phi1*dot(self.zgradcalc,self.phi1.grad) + self.phi2*dot(self.zgradcalc,self.phi2.grad))))

        self.energychi = 8.*pi*self.mp.M5tL/self.z**3 * (self.r**2/4.*((dot(self.rgradcalc,self.chi1.grad)+self.Ar*self.chi2)**2 + (dot(self.zgradcalc,self.chi1.grad)+self.Az*self.chi2)**2 \
              + (dot(self.rgradcalc,self.chi2.grad)-self.Ar*self.chi1)**2 + (dot(self.zgradcalc,self.chi2.grad)-self.Az*self.chi1)**2 - self.pg.dzchi0(self.z)**2) \
              + 1./2.*(self.phi1*self.chi2-self.chi1*self.phi2)**2 \
              + self.r**2/(4.*self.z**2)*(self.mp.Mbulk2tL2 * (self.chi1**2 + self.chi2**2 - self.pg.chi0(self.z)**2)))

        from fipy.tools.numerix import sign

        if self.mp.chicoeff == 0:
            self.energy = (self.energyphi + self.energyCS) * (1.+sign(self.z-self.cp.zeps-2.*self.cp.meshdelta))/2.
        else:
            #self.energy = (self.energyphi + self.energyCS + self.energychi)
            self.energy = (self.energyphi + self.energyCS + self.energychi) * (1.+sign(self.z-self.cp.zeps-2.*self.cp.meshdelta))/2. \
                * (1.-sign(self.r-(self.cp.Rm*1.)))/2.
        self.energy.name = 'Energy density'

        self.gauge = dot(self.rgradcalc,self.Ar.grad)+dot(self.zgradcalc,self.Az.grad)
        self.chideltad = self.r**2/self.z**3 * ((dot(self.rgradcalc,self.chi1.grad)+self.Ar*self.chi2)**2 + (dot(self.zgradcalc,self.chi1.grad)+self.Az*self.chi2)**2 \
              + (dot(self.rgradcalc,self.chi2.grad)-self.Ar*self.chi1)**2 + (dot(self.zgradcalc,self.chi2.grad)-self.Az*self.chi1)**2 - self.pg.dzchi0(self.z)**2)

        self.gauge.name = 'Gauge density'

        ############################   Equation variables ################################################

        self.cutbound = (1.+sign(self.z-self.cp.zeps-2.*self.cp.meshdelta)) * (1.+sign(self.r - self.cp.Reps - 2.*self.cp.meshdelta)) \
                      * (1.+sign(self.cp.zm - 2.*self.cp.meshdelta - self.z)) * (1.+sign(self.cp.Rm - 2.*self.cp.meshdelta-self.r))/16.
        self.grr = [[1],[0]]
        self.grz = [[0],[1]]


        self.Drphi1 = dot(self.grr, self.phi1.grad) + self.Ar * self.phi2
        self.Dzphi1 = dot(self.grz, self.phi1.grad) + self.Az * self.phi2
        self.Drphi2 = dot(self.grr, self.phi2.grad) - self.Ar * self.phi1
        self.Dzphi2 = dot(self.grz, self.phi2.grad) - self.Az * self.phi1

        self.Drchi1 = dot(self.grr, self.chi1.grad) + self.Ar * self.chi2
        self.Dzchi1 = dot(self.grz, self.chi1.grad) + self.Az * self.chi2
        self.Drchi2 = dot(self.grr, self.chi2.grad) - self.Ar * self.chi1
        self.Dzchi2 = dot(self.grz, self.chi2.grad) - self.Az * self.chi1

        self.dmuAmu = dot(self.grr,self.Ar.grad) + dot(self.grz,self.Az.grad)
        self.Amusqr = self.Ar**2 + self.Az**2

        self.phicurr_r = -2.*(self.phi1**2+self.phi2**2)*self.Ar + \
                          2.*(self.phi1*dot(self.grr,self.phi2.grad)-self.phi2*dot(self.grr,self.phi1.grad))

        self.phicurr_z = -2.*(self.phi1**2+self.phi2**2)*self.Az + \
                          2.*(self.phi1*dot(self.grz,self.phi2.grad)-self.phi2*dot(self.grz,self.phi1.grad))

        self.chicurr_r = -2.*(self.chi1**2+self.chi2**2)*self.Ar + \
                          2.*(self.chi1*dot(self.grr,self.chi2.grad)-self.chi2*dot(self.grr,self.chi1.grad))

        self.chicurr_z = -2.*(self.chi1**2+self.chi2**2)*self.Az + \
                          2.*(self.chi1*dot(self.grz,self.chi2.grad)-self.chi2*dot(self.grz,self.chi1.grad))



        self.eqvarphi1 = 1./self.z * self.Dzphi1 - self.phi1.faceGrad.divergence \
          - 2.*(dot(self.grr,self.phi2.grad)*self.Ar + dot(self.grz,self.phi2.grad) * self.Az) \
          - self.dmuAmu * self.phi2 + self.Amusqr * self.phi1 \
          - self.phi1/self.r**2 * (1.-self.phi1**2-self.phi2**2) \
          - self.z*self.mp.gamma * ((dot(self.grr,self.s.grad)/self.r - self.s/self.r**2) \
                                     * (-dot(self.grz, self.phi2.grad) + self.Az*self.phi1) \
                             - dot(self.grz,self.s.grad)/self.r * (-dot(self.grr, self.phi2.grad) + self.Ar*self.phi1)) \
          + self.mp.chicoeff / 2. / self.z**2 * self.chi2 * (self.phi1*self.chi2 - self.chi1*self.phi2)

        self.eqvarphi1.name = 'Phi1 equation residual'


        self.eqvarphi2 = 1./self.z * self.Dzphi2 - self.phi2.faceGrad.divergence \
          + 2.*(dot(self.grr,self.phi1.grad)*self.Ar + dot(self.grz,self.phi1.grad) * self.Az) \
          + self.dmuAmu * self.phi1 + self.Amusqr * self.phi2 \
          - self.phi2/self.r**2 * (1.-self.phi1**2-self.phi2**2) \
          - self.z*self.mp.gamma * ((dot(self.grr,self.s.grad)/self.r - self.s/self.r**2) \
                                     * (dot(self.grz, self.phi1.grad) + self.Az*self.phi2) \
                             - dot(self.grz,self.s.grad)/self.r * (dot(self.grr, self.phi1.grad) + self.Ar*self.phi2)) \
          - self.mp.chicoeff / 2. / self.z**2 * self.chi1 * (self.phi1*self.chi2 - self.chi1*self.phi2)

        self.eqvarphi2.name = 'Phi2 equation residual'


        self.eqvars = -1./self.z * dot(self.grz,self.s.grad) + self.s.faceGrad.divergence \
                    - self.mp.gamma * self.z / self.r * ((dot(self.grr,self.Az.grad) - dot(self.grz,self.Ar.grad)) \
                           + (2.*dot(self.grr,self.phi1.grad)*dot(self.grz,self.phi2.grad) - 2.*dot(self.grr,self.phi2.grad)*dot(self.grz,self.phi1.grad) \
                           - dot(self.grr,((self.phi1**2+self.phi2**2)*self.Az).grad) + dot(self.grz,((self.phi1**2+self.phi2**2)*self.Ar).grad)))

        self.eqvars.name = 'S equation residual'


        self.eqvarstest = -1./self.z * dot(self.grz,self.s.grad) + self.s.faceGrad.divergence \
                        - self.mp.gamma * self.z / self.r / 2. * \
                        ( 2.*(dot(self.grr,self.Az.grad) - dot(self.grz,self.Ar.grad))
                          + dot(self.grr,self.phicurr_z.grad) - dot(self.grz,self.phicurr_r.grad))

        self.eqvarstest.name = 'S equation residual alt'


        self.eqvarAr = - self.phicurr_r + self.r**2/self.z * (dot(self.grz,self.Ar.grad)-dot(self.grr,self.Az.grad)) \
                       - self.r**2 * self.Ar.faceGrad.divergence + self.r**2 * dot(self.grr,self.dmuAmu.grad) \
                       + self.mp.gamma * self.z * (- dot(self.grz,self.s.grad)/self.r) * (1-self.phi1**2 - self.phi2**2) \
                       - self.mp.chicoeff * self.r**2/4./self.z**2 * self.chicurr_r

        self.eqvarAr.name = 'Ar equation residual'



        self.eqvarAz = - self.phicurr_z - 2.*self.r * (dot(self.grr,self.Az.grad)-dot(self.grz,self.Ar.grad)) \
                       - self.r**2 * self.Az.faceGrad.divergence + self.r**2 * dot(self.grr,self.dmuAmu.grad) \
                       + self.mp.gamma * self.z * (dot(self.grr,self.s.grad)/self.r - self.s/self.r**2) * (1-self.phi1**2 - self.phi2**2) \
                       - self.mp.chicoeff * self.r**2/4./self.z**2 * self.chicurr_z


        self.eqvarAz.name = 'Az equation residual'

        self.eqvarchi1 = 3./self.z * self.Dzchi1 - 2./self.r * self.Drchi1 - self.chi1.faceGrad.divergence \
          - 2.*(dot(self.grr,self.chi2.grad)*self.Ar + dot(self.grz,self.chi2.grad) * self.Az) \
          - self.dmuAmu * self.chi2 + self.Amusqr * self.chi1 \
          - 2. / self.r**2 * self.phi2 * (self.phi1*self.chi2 - self.chi1*self.phi2) \
          + 1. / self.z**2 * self.mp.Mbulk2tL2 * self.chi1

        self.eqvarchi1.name = 'Chi1 equation residual'

        self.eqvarchi2 = 3./self.z * self.Dzchi2 - 2./self.r * self.Drchi2 - self.chi2.faceGrad.divergence \
          + 2.*(dot(self.grr,self.chi1.grad)*self.Ar + dot(self.grz,self.chi1.grad) * self.Az) \
          + self.dmuAmu * self.chi1 + self.Amusqr * self.chi2 \
          + 2. / self.r**2 * self.phi1 * (self.phi1*self.chi2 - self.chi1*self.phi2) \
          + 1. / self.z**2 * self.mp.Mbulk2tL2 * self.chi2

        self.eqvarchi2.name = 'Chi2 equation residual'

        ###########################   Micsellaneous variables ############################################

        self.chideltapot = self.r**2/self.z**5 * (self.chi1**2 + self.chi2**2 - self.pg.chi0(self.z)**2)

        self.r2se = -self.Lfem**2/pi/self.mp.gamma * self.r**3 / self.z * dot(self.zgradfix,self.s.grad)
        self.muv = 3.*1090. * self.mp.L / (9*pi*self.mp.gamma) * self.r**2 / self.z \
                 * (dot(self.zgradfix,self.phi2.grad) - self.Az * self.phi1)
        self.muvraw = 3.*self.getEnergy() * self.mp.L / (9*pi*self.mp.gamma) * self.r**2 / self.z \
                 * (dot(self.zgradfix,self.phi2.grad) - self.Az * self.phi1)
        self.ga  = -3./(9.*pi*self.mp.gamma)*self.r/self.z*(2.*(dot(self.zgradfix,self.phi1.grad) + self.Az * self.phi2) \
                 + self.r*(dot(self.zgradfix,self.Ar.grad) - dot(self.rgradfix,self.Az.grad) ))

        self.r2se.name = 'R2e,s density'
        self.muv.name = 'Muv density'
        self.ga.name = 'Ga density'

    def getEnergy(self):
        return sum(self.energy.numericValue) * self.cp.meshdelta**2 / self.mp.L
    def getTopCharge(self):
        return sum(self.topcharge.numericValue) * self.cp.meshdelta**2 / 2. / pi

    def intvarold(self,ocp,ovar):
        return lambda r,z: ovar((r-ocp.Reps,z-ocp.zeps))

    def intvar(self,cc,ovar):
        return lambda r,z: ovar.value[cc.ijton(cc.rtoi(r),cc.ztoj(z))]

    def setvarold(self,cp,var,func):
        printlog('Interpolating %s'%var.name)
        maxi = var.mesh.nx - 1
        d = 0
        for i in range(0,var.mesh.nx):
            if 10.*i/maxi > d:
                print ('%i%%'%(d*10))
                d += 1
            for j in range(0,var.mesh.ny):
                var.value[j*var.mesh.nx+i]=func(r=cp.Reps+var.mesh.dx*i + var.mesh.dx/2.,
                                              z=cp.zeps+var.mesh.dy*j + var.mesh.dy/2.)

    def setvar(self,cc,var,func):
        printlog('Interpolating %s'%var.name)
        maxi = cc.nr - 1
        d = 0
        for i in range(0,cc.nr):
            if 10.*i/maxi > d:
                print ('%i%%'%(d*10))
                d += 1
            for j in range(0,cc.nz):
                var.value[j*cc.nr+i]=func(r=cc.itor(i),z=cc.jtoz(j))


    def intpsi1(self,r,z,ocp,opsi1):
        if (z < ocp.zeps):
            return 0.
        if (r > ocp.Rm):
            return sin(np.pi*(z-ocp.zeps)/(ocp.zm-ocp.zeps))/r
        return opsi1(r,z)

    def intpsi2(self,r,z,ocp,opsi2):
        if (z < ocp.zeps):
            return 0.
        if (r > ocp.Rm):
            return (1.-cos(np.pi*(z-ocp.zeps)/(ocp.zm-ocp.zeps)))/r
        return opsi2(r,z)

    def intAr(self,r,z,ocp,oAr):
        if (z < ocp.zeps): return 0.
        if (r > ocp.Rm): return oAr(ocp.Rm,z)
        return oAr(r,z)

    def intAz(self,r,z,ocp,oAz):
        if (r > ocp.Rm): return np.pi/(ocp.zm-ocp.zeps)
        if (z < ocp.zeps): return oAz(r,ocp.zeps)
        return oAz(r,z)

    def ints(self,r,z,ocp,oS):
        if (r > ocp.Rm): return 0.
        if (z < ocp.zeps): return 0.
        return oS(r,z)

    def intchi1(self,r,z,ocp,opg,ochi1):
        if (z < ocp.zeps): return 0.
        if (r > ocp.Rm): return - opg.chi0(z) * sin(np.pi*(z-ocp.zeps)/(ocp.zm-ocp.zeps))
        return ochi1(r,z)

    def intchi2(self,r,z,ocp,opg,ochi2):
        if (z < ocp.zeps): return opg.chi0(ocp.zeps)
        if (r > ocp.Rm): return  opg.chi0(z) * cos(np.pi*(z-ocp.zeps)/(ocp.zm-ocp.zeps))
        return ochi2(r,z)

    def __old_save(self,filename=None):
        if filename is None:
           filename = 'nres_%.1f_%.3f_%.3f.sv'%(self.cp.Rm,self.cp.zeps,self.cp.meshdelta)
        printlog('Saving result')
        with io.open(filename,mode='w') as handle:
          handle.write(u'#format: cp, mp, psi1, psi2, Ar, Az, s, chi1, chi2\n')
          handle.write(u'%s\n'%json.dumps(self.cp.serialize()))
          handle.write(u'%s\n'%json.dumps(self.mp.serialize()))
          for var in [self.psi1,self.psi2,self.Ar,self.Az,self.s,self.chi1,self.chi2]:
              arr = []
              for i in range(0,self.mesh.nx*self.mesh.ny): arr.append(var.value[i])
              handle.write(u'%s\n'%json.dumps(arr))


        printlog('Saved result as %s'%filename)

    def interpolatedata(self,oldcw):
        printlog('Interpolating')
        if (oldcw.cp.Reps != self.cp.Reps) or (oldcw.cp.zm != self.cp.zm) \
          or (oldcw.cp.Rm > self.cp.Rm) or (oldcw.cp.zeps < self.cp.zeps):
            printlog('Calculation parameters too different, cannot interpolate; exiting')
            return

        oldcc = coordConv(oldcw.cp,oldcw.mesh)
        newcc = coordConv(self.cp,self.mesh)

        self.setvar(newcc,self.psi1,lambda r,z: self.intpsi1(r,z,oldcw.cp,self.intvar(oldcc,oldcw.psi1)))
        self.setvar(newcc,self.psi2,lambda r,z: self.intpsi2(r,z,oldcw.cp,self.intvar(oldcc,oldcw.psi2)))
        self.setvar(newcc,self.Ar,lambda r,z: self.intAr(r,z,oldcw.cp,self.intvar(oldcc,oldcw.Ar)))
        self.setvar(newcc,self.Az,lambda r,z: self.intAz(r,z,oldcw.cp,self.intvar(oldcc,oldcw.Az)))
        self.setvar(newcc,self.s,lambda r,z: self.ints(r,z,oldcw.cp,self.intvar(oldcc,oldcw.s)))
        self.setvar(newcc,self.chi1,lambda r,z: self.intchi1(r,z,oldcw.cp,oldcw.pg,self.intvar(oldcc,oldcw.chi1)))
        self.setvar(newcc,self.chi2,lambda r,z: self.intchi2(r,z,oldcw.cp,oldcw.pg,self.intvar(oldcc,oldcw.chi2)))

    def oldcalculate(self,precgoal=1e-6):
        i = 0
        printlog('Calculating with the precision goal of %f'%precgoal)
        while True:
            i += 1
            if abs(self.step(dt=dtfun(i),stepnum=i))<precgoal:
                break
        printlog('Calculation finished!')

    def getolddiff(self):
        diffpsi1 = self.psi1.value - self.psi1.old
        dp1max = abs(diffpsi1.max())
        dp1min = abs(diffpsi1.min())

        if self.mp.chicoeff != 0:
            vs = [self.psi1,self.psi2,self.Ar,self.Az,self.s,self.chi1,self.chi2]
        else:
            vs = [self.psi1,self.psi2,self.Ar,self.Az,self.s]

        maxdiff = 0.
        mvar = self.psi1
        for var in vs:
            diffvar = var.value - var.old
            diffmax=diffvar.max()
            diffmin=diffvar.min()
            if abs(maxdiff) < abs(diffmax):
                maxdiff = diffmax
                mvar = var
            if abs(maxdiff) < abs(diffmin):
                maxdiff = diffmin
                mvar = var

        return (maxdiff,mvar)

    def geteqres(self,cutbound = True):
        if self.mp.chicoeff != 0:
            vs = [self.eqvarphi1,self.eqvarphi2,self.eqvarAr,self.eqvarAz,self.eqvars,self.eqvarchi1,self.eqvarchi2]
        else:
            vs = [self.eqvarphi1,self.eqvarphi2,self.eqvarAr,self.eqvarAz,self.eqvars]

        eqres = []

        for var in vs:
            vdiff = 0.
            if cutbound: vmax = (var*self.cutbound).max()
            else: vmax = (var).max()
            if cutbound: vmin = (var*self.cutbound).min()
            else: vmax = (var).min()
            if abs(vmax) > abs(vdiff): vdiff = vmax
            if abs(vmin) > abs(vdiff): vdiff = vmin
            eqres.append((var,vdiff))

        return eqres

    def updatevars(self):
        self.psi1.updateOld()
        self.psi2.updateOld()
        self.Ar.updateOld()
        self.Az.updateOld()
        self.s.updateOld()
        if self.mp.chicoeff != 0:
            self.chi1.updateOld()
            self.chi2.updateOld()

    def step(self,dt,wait=False,stepnum=None,nsweeps = 1,sweeprelpres=None,sweepsilent=False):
        if stepnum is None: printlog('Calculating next step')
        else: printlog('Calculating step %i'%stepnum)
        self.updatevars()


        if sweeprelpres is None:
          for i in range(0,nsweeps):
            residual = self.eqsystemn.sweep(dt=dt)
            if not sweepsilent: printlog('Sweep residual: %g'%residual)

        else:
          printlog('Sweeping to the relative precision of %g'%sweeprelpres)
          startresidual = self.eqsystemn.sweep(dt=dt)
          residual = startresidual
          if not sweepsilent: printlog('Sweep residual: %g'%residual)
          while residual > startresidual * sweeprelpres:
            residual = self.eqsystemn.sweep(dt=dt)
            if not sweepsilent: printlog('Sweep residual: %g'%residual)



        printlog('Step completed!')
        currEnergy = self.getEnergy()
        printlog('Topological charge: %.6f, energy: %.4f MeV'%(self.getTopCharge(),currEnergy))
        (diff,diffvar) = self.getolddiff()
        printlog('Max field residual: %g in the field %s'%(diff,diffvar.name))


        for plot in self.plots:
            plot.plot()
        if wait:
          try:
            input("Press enter to continue")
          except SyntaxError:
            pass

        return diff


    def calculate(self,desiredprecision=1e-6,fixedtimestep=None,sweeptopres = False,startstep = 0):

        fl = fcntl.fcntl(sys.stdin.fileno(), fcntl.F_GETFL)
        fcntl.fcntl(sys.stdin.fileno(), fcntl.F_SETFL, fl | os.O_NONBLOCK)


        i = startstep

        maxprestimestepincr = 1.

        precdiff = 0.1

        timest = time()
        self.cp.pr()
        self.mp.pr()
        printlog('Calculating with the precision goal of %g'%desiredprecision)
        self.plots = []
        self.plots.append(Viewer(vars=self.energy))

        if fixedtimestep is None:
            dt = dtfun(i)
        else:
            dt = fixedtimestep

        if sweeptopres == False:
          prevprec = abs(self.step(dt=dt,stepnum=i,nsweeps=1))
        else:
          prevprec = abs(self.step(dt=dt,stepnum=i,sweeprelpres=0.5))
        while True:
            i += 1

            if fixedtimestep is None:
               dt = dtfun(i)
            else:
               dt = fixedtimestep

            if i > 0:
              prec = abs(self.step(dt=dt,stepnum=i,nsweeps=1))
            else:
              prec = abs(self.step(dt=dt,stepnum=i,sweeprelpres=0.5))
            prevprec = prec
            if not checkkeyinput(): break

            if prec < desiredprecision:
                break


        printlog('Calculation finished!')
        self.cp.pr()
        self.mp.pr()
        printlog('Number of steps: %i, achieved precision: %g'%(i,prec))
        return(i,prec,time()-timest)

def __old_loadwrapper(filename):
    with io.open(filename,mode='r') as handle:
      handle.readline()
      line = handle.readline()
      cp = defcp
      cp.deserialize(json.loads(line))
      line = handle.readline()
      mp = defmp
      mp.deserialize(json.loads(line))

      cw = calcWrapper(cp,mp)

      for var in [cw.psi1,cw.psi2,cw.Ar,cw.Az,cw.s,cw.chi1,cw.chi2]:
          line = handle.readline()
          array = json.loads(line)
          for i in range(0,cw.mesh.nx*cw.mesh.ny): var.value[i] = array[i]

    return (cw)




def dtfun(stepnum):
    #return math.sqrt(1./(stepnum+1.))/3.
    return 0.01+0.0001*stepnum


def solanalyze(cw,plot=False,pr = False):

    energy = cw.getEnergy()

    if plot:
      vr = Viewer(cw.energy)
      vr.plot()

    vr2se = vis1d(cp=cw.cp,axis='r',var=cw.r2se,coord=cw.cp.zeps+2.*cw.cp.meshdelta,plot=plot)
    vmuv = vis1d(cp=cw.cp,axis='r',var=cw.muv,coord=cw.cp.zeps+2.*cw.cp.meshdelta,plot=plot)
    vga = vis1d(cp=cw.cp,axis='r',var=cw.ga,coord=cw.cp.zeps+2.*cw.cp.meshdelta,plot=plot)

    r2sesqr = math.sqrt(vr2se.integrate())
    muv = vmuv.integrate()
    ga = vga.integrate()

    eqres = cw.geteqres()

    if pr:

      cw.cp.pr()
      cw.mp.pr()
      printlog('')
      printlog('Energy: %g'%energy)
      printlog('')
      printlog('Equations resuduals:')


      for (var,diff) in eqres:
        printlog('%s : diff %g'%(var.name,diff))



      printlog('')
      printlog("Sqr r2e,s: %f fm; expected 0.82 fm"%r2sesqr)

      printlog("Muv: %f; expected 1.18"%muv)

      printlog("Ga: %f; expected 0.58"%ga)

    outp=[]
    outp.append((energy,'Energy'))
    for (var,diff) in eqres:
      outp.append((float(diff),'%s diff'%var.name))
    #outp.append((energy,'Energy,MeV')
    outp.append((r2sesqr,'Sqr r2e,s fm'))
    outp.append((muv,'Muv'))
    outp.append((ga,'Ga'))

    return outp

def checkkeyinput():
    try:
        stdin = sys.stdin.read()
        if "\n" in stdin or "\r" in stdin:
          print ('Stop the calculation? Wating 10 seconds for an answer')
          for j in range(9,0,-1):
            sleep(1)
            print(j)
          sleep(1)
          try:
            stdin = sys.stdin.read()
            if "\n" in stdin or "\r" in stdin:
              print ('Stopping calculation')
              return False
          except IOError: pass
          print('Continuing calculation')
    except IOError: pass
    return True


defcp = calcParam(Reps=0.,Rm=4.,zeps=0.05, zm=1.,meshdelta=0.01,sweepres=0.)
# RMSE
mp_rmse = modelParam(L=1./325.0,M5tL=0.014,mq=31.,xitL=2.1,Mbulk2tL2=-3.7,chicoeff=1.)
# max deviation
mp_maxdev = modelParam(L=1./350.,M5tL=0.014,mq=40.,xitL=1.5,Mbulk2tL2=-3.8,chicoeff=1.)
# physical
#mp_phys = modelParam(L=1./
#old soliton
mp_oldsol = modelParam(L=1./350.,M5tL=0.017,mq=31.,xitL=2.,Mbulk2tL2=-3.,chicoeff=0.,gamma=1.27)
