import solcalc
from fipy import *
from time import time
import io

mp = solcalc.defmp
cp = solcalc.defcp

mp.chicoeff = 0.
cp.Rm = 6.
cp.meshdelta = 0.005


#cwmd = solcalc.calcWrapper(cp,mp)

#cwmd.cp.pr()
#cwmd.calculate(desiredprecision=1e-8,sweeptopres=False)

#cwmd.save('chi0_rm6_md0005_prec1e-8.sv')

mp.chicoeff = 1.
cp.Rm = 6.
cp.meshdelta = 0.01


#cwmd = solcalc.calcWrapper(cp,mp)

#cwmd.cp.pr()
#cwmd.calculate(desiredprecision=1e-8,sweeptopres=False)

#cwmd.save('chi1_rm6_md001_prec1e-8.sv')


mp.chicoeff = 1.
cp.Rm = 6.
cp.meshdelta = 0.005


cwmd = solcalc.calcWrapper(cp,mp)

cwmd.cp.pr()
cwmd.calculate(desiredprecision=1e-6,sweeptopres=False)

cwmd.save('chi1_rm6_md0005_prec1e-6.sv')